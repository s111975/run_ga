import csv
import subprocess
import re
from random import randrange
from time import time as t

def run():
    output_dir = 'csv/'
    run_params = readFile("run_params.csv")
    for case in run_params:
        for i in range(15):
            print("time:", t(), "iteration:", i)
            seed = randrange(100000)
            result = subprocess.run(
                ["java", "-jar", "sysopt-project.jar", case[0], case[1], case[2], str(seed), case[4]],
                capture_output=True
            )
            # print("result = ", result.stdout.decode('utf-8'))
            valid = re.findall(r'SuccessfulRuns = \d+', result.stdout.decode('utf-8'))[0].split(' ')[-1]
            time = re.findall(r'CPUTime_Mean = \d+', result.stdout.decode('utf-8'))[0].split(' ')[-1]
            steps = re.findall(r'Steps_Mean = \d+', result.stdout.decode('utf-8'))[0].split(' ')[-1]
            length = re.findall(r'Length = \d+', result.stdout.decode('utf-8'))[0].split(' ')[-1]
            overlap = re.findall(r'Overlap = \d+', result.stdout.decode('utf-8'))[0].split(' ')[-1]
            critical = re.findall(r'CriticalLinks = \d+', result.stdout.decode('utf-8'))[0].split(' ')[-1]
            data = [((seed, valid, time, steps, length, overlap, critical))]
            writeData(data, output_dir+case[4]+'csv')
            i = i + 1

def readFile(filename):
    with open(filename, newline='') as csvfile:
        data = list(csv.reader(csvfile))
    return data

def writeData(data, filename):
        with open(filename, 'a') as csvfile:
            filewriter = csv.writer(csvfile)
            for r in data:
                filewriter.writerow(r)


if __name__ == "__main__":
    run()